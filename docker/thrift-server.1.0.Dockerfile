# syntax=docker/dockerfile:1
FROM openjdk:16-alpine3.13
WORKDIR /thrift
ADD "target/thrift-server-1.0.jar" ./
EXPOSE 9090
CMD ["java", "-jar", "thrift-server-1.0.jar"]