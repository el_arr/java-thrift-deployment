# syntax=docker/dockerfile:1
FROM openjdk:16-alpine3.13
WORKDIR /thrift
ADD "target/thrift-client-1.0.jar" ./
EXPOSE 9090
CMD java -jar thrift-client-1.0.jar $SERVER_IP